##
## input.py for BASIC-RoBots.py
##  
## Copyright (C) 2012 Pierre Surply
## <pierre.surply@gmail.com>
##
## This file is part of BASIC-RoBots.
##
##    BASIC-RoBots is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    BASIC-RoBots is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with BASIC-RoBots.  If not, see <http://www.gnu.org/licenses/>.
##
## Started on  Wed May  9 17:54:23 2012 Pierre Surply
## Last update Sun Aug 12 16:44:39 2012 Pierre Surply
##

import pygame
from pygame.locals import *

from collections import deque

class Input:
    def __init__(self):
        pygame.key.set_repeat(500, 30)
        self.key = [0]*1000
        self.mouse = (0,0)
        self.mouserel = (0,0)
        self.mousebuttons= [0]*9
        self.quit = False
        self.char_stack = deque([])
        self.rec_char = False

    def update(self, display):
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                self.key[event.key] = 1;
                if self.rec_char:
                    self.push_char(event.unicode)
            elif event.type == KEYUP:
                self.key[event.key] = 0;
            elif event.type == MOUSEMOTION:
                self.mouse = event.pos
                self.mouserel = event.rel
            elif event.type == MOUSEBUTTONDOWN:
                self.mousebuttons[event.button] = 1
            elif event.type == MOUSEBUTTONUP:
                if event.button != 4 and event.button != 5:
                    self.mousebuttons[event.button] = 0
            elif event.type == QUIT:
                self.quit = True
            elif event.type == VIDEORESIZE:
                display.resize(event.size)

    def push_char(self, c):
        self.char_stack.append(c)
        
    def get_key_once(self, k):
        if self.key[k]:
            self.key[k] = False
            return True
        else:
            return False
